require 'date'

class Meetup

  attr_reader :year, :month

  week_identifier = [
    "first",
    "second" ,
    "teenth",
    "third",
    "fourth",
    'last'
  ]
  

  def initialize(month, year)
    @year = year
    @month = month
  end

  def day(day_of_week, week_identifier)
    #Converting String nameof month to integer
    monthString = Date::MONTHNAMES.index(month) 
    # get the all date on numercial format
    candidate_date = Date.new(year, monthString, start_day_for_week(week_identifier))
    return candidate_date
  end

  def start_day_for_week(week_identifier)
    case week_identifier
    when "first"
      1
    when "second"
      8
    when "teenth"
      13
    when "third"
      15
    when "fourth"
      22
    when "last"
      Date.new(year, month).next_month.prev_day.day - 6
    end
  end
end



puts ('---------------------')

test = Meetup.new('January',2017)
puts test.day(1,"first")

puts ('---------------------')

test = Meetup.new('March',2017)
puts test.day(2,"third")

puts ('---------------------')