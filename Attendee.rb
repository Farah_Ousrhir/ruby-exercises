class Attendee

    def initialize(height)
      @height = height
    end

    attr_reader :height, :pass_id

    def issue_pass!(_pass_id)
      @pass_id = _pass_id
    end

    def revoke_pass!
      @pass_id = nil
    end
    
  end

# Make new attendees
puts Attendee.new(106)

# How tall is the attendee
puts Attendee.new(106).height

# What is the ride pass id
puts Attendee.new(106).pass_id

# Allow people to buy a pass
attendee = Attendee.new(106)
attendee.issue_pass!(42)
puts attendee.pass_id

# Revoke the pass
attendee = Attendee.new(106)
attendee.issue_pass!(42)
attendee.revoke_pass!
puts attendee.pass_id
